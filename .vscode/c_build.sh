#!/bin/bash

# Only execute make when there are changes
make -q
if [[ $? -ne 0 ]]; then
    make debug
fi
