# [Sudoku Solver](https://gitlab.com/debemdeboas/sudoku-solver)

This repository contains the code for my Sudoku puzzle solver written in C.
Originally, this was written for a class in my Computer Science bachelor's, but it's evolved into its own independent project.
