#include "SudokuBoard.h"
#include "util.h"
#include <emscripten.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define MAX_BUFFER_SIZE 4096
#define SOCKET_SERVER_PORT 4040

EMSCRIPTEN_KEEPALIVE
SudokuBoard *load_from_stream_js(int *board_data) {
    SudokuBoard *board = malloc(sizeof(SudokuBoard));
    memset(board, 0, sizeof(SudokuBoard));

    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            board->data[i][j] = *board_data;
            board_data++;
        }
    }

    return board;
}
