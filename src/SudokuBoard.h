#ifndef SUDOKU_BOARD_H_
#define SUDOKU_BOARD_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BOARD_SIZE 9
#define SUB_BOARD_SIZE 3
#define PERFORATION_CHANCE_PERCENTAGE 70
#define SHUFFLING_ATTEMPTS 300
#define NUM_SOLUTIONS 10

typedef struct {
    int data[BOARD_SIZE][BOARD_SIZE];
} SudokuBoard;

typedef struct {
    SudokuBoard *array;
    int size;
    int capacity;
} SudokuBoardArray;

SudokuBoardArray *create_sudoku_board_array(const int initial_capacity);

void append_to_array(SudokuBoardArray *arr, SudokuBoard board);

void free_array(SudokuBoardArray *arr);

bool is_valid(const SudokuBoard *board, int row, int col, int num);

bool is_board_valid(SudokuBoard *board);

bool generate_board(SudokuBoard *board, int row, int col, const int num_order[BOARD_SIZE]);

void shuffle_full_board(SudokuBoard *board);

void shuffle_board(SudokuBoard *board);

void shuffle_within_block(int data[BOARD_SIZE][BOARD_SIZE], int start, int end, bool is_row);

void swap_rows(int data[BOARD_SIZE][BOARD_SIZE], int row1, int row2);

void swap_columns(int data[BOARD_SIZE][BOARD_SIZE], int col1, int col2);

void poke_holes_in_board(SudokuBoard *board, SudokuBoard *perforated_board);

void duplicate_board(SudokuBoard *original, SudokuBoard *copy);

int save_sudoku_board(SudokuBoard *board, const char *file_name);

void print_sudoku_board(SudokuBoard *board);

SudokuBoardArray *solve(SudokuBoard *board);

void scramble_num_order(int *num_order);

void generate_sudoku_board(SudokuBoard *result);

#endif // SUDOKU_BOARD_H_