#include "SudokuBoard.h"

/**
 * Verify if a move is valid
 * @return True if the move is valid, false otherwise
*/
bool is_valid(const SudokuBoard *board, int row, int col, int num) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        if (board->data[row][i] == num || board->data[i][col] == num) {
            return false;
        }
    }

    int row_start = SUB_BOARD_SIZE * (row / SUB_BOARD_SIZE);
    int col_start = SUB_BOARD_SIZE * (col / SUB_BOARD_SIZE);
    for (int i = 0; i < SUB_BOARD_SIZE; i++) {
        for (int j = 0; j < SUB_BOARD_SIZE; j++) {
            if (board->data[row_start + i][col_start + j] == num) {
                return false;
            }
        }
    }

    return true;
}

/**
 * Verify if the given Sudoku board is valid or not
 * @return True if the board is valid, false otherwise
*/
bool is_board_valid(SudokuBoard *board) {
    for (int i = 0; i < BOARD_SIZE; ++i) {
        for (int j = 0; j < BOARD_SIZE; ++j) {
            int num = board->data[i][j];
            // Temporarily remove the number from the cell
            board->data[i][j] = 0;
            // Check if the number was valid in this position
            if (!is_valid(board, i, j, num)) {
                // If not, put the number back and return false
                board->data[i][j] = num;
                return false;
            }
            // Put the number back
            board->data[i][j] = num;
        }
    }
    return true;
}

/**
 * Generate a valid Sudoku board
*/
bool generate_board(SudokuBoard *board, int row, int col, const int num_order[BOARD_SIZE]) {
    if (row == BOARD_SIZE) {
        shuffle_full_board(board);
        return true;
    }

    int next_row = col < BOARD_SIZE - 1 ? row : row + 1;
    int next_col = col < BOARD_SIZE - 1 ? col + 1 : 0;

    if (board->data[row][col] != 0) {
        return generate_board(board, next_row, next_col, num_order);
    } else {
        for (int i = 0; i < BOARD_SIZE; i++) {
            int num = num_order[i];
            if (is_valid(board, row, col, num)) {
                board->data[row][col] = num;
                if (generate_board(board, next_row, next_col, num_order)) {
                    return true;
                }
                board->data[row][col] = 0;
            }
        }
        return false;
    }
}

/**
 * Complete shuffle of the board and mainain its solvability
*/
void shuffle_full_board(SudokuBoard *board) {
    shuffle_board(board);
    int attempts = SHUFFLING_ATTEMPTS; // Number of attempts to shuffle the entire board
    while (attempts--) {
        int row1 = rand() % BOARD_SIZE;
        int row2 = rand() % BOARD_SIZE;
        swap_rows(board->data, row1, row2);
        if (!is_board_valid(board)) {
            // Swap back if board is invalid
            swap_rows(board->data, row1, row2);
        }

        int col1 = rand() % BOARD_SIZE;
        int col2 = rand() % BOARD_SIZE;
        swap_columns(board->data, col1, col2);
        if (!is_board_valid(board)) {
            // Swap back if board is invalid
            swap_columns(board->data, col1, col2);
        }
    }
}

/**
 * Shuffle the board within a 3x3 region
*/
void shuffle_within_block(int data[BOARD_SIZE][BOARD_SIZE], int start, int end, bool is_row) {
    int j, temp;
    for (int i = start; i < end; ++i) {
        j = rand() % (end - start) + start;
        if (is_row) {
            for (int k = 0; k < BOARD_SIZE; ++k) {
                temp = data[i][k];
                data[i][k] = data[j][k];
                data[j][k] = temp;
            }
        } else {
            for (int k = 0; k < BOARD_SIZE; ++k) {
                temp = data[k][i];
                data[k][i] = data[k][j];
                data[k][j] = temp;
            }
        }
    }
}

/**
 * Shuffle the board as best we can
*/
void shuffle_board(SudokuBoard *board) {
    // Shuffle rows within each block
    for (int block = 0; block < BOARD_SIZE; block += SUB_BOARD_SIZE) {
        shuffle_within_block(board->data, block, block + SUB_BOARD_SIZE, true);
    }

    // Shuffle blocks of rows
    for (int block = 0; block < BOARD_SIZE; block += SUB_BOARD_SIZE) {
        int swap_block = (rand() % (BOARD_SIZE / SUB_BOARD_SIZE)) * SUB_BOARD_SIZE;
        if (block != swap_block) {
            for (int row = 0; row < SUB_BOARD_SIZE; ++row) {
                swap_rows(board->data, block + row, swap_block + row);
            }
        }
    }

    // Shuffle columns within each block
    for (int block = 0; block < BOARD_SIZE; block += SUB_BOARD_SIZE) {
        shuffle_within_block(board->data, block, block + SUB_BOARD_SIZE, false);
    }

    // Shuffle blocks of columns
    for (int block = 0; block < BOARD_SIZE; block += SUB_BOARD_SIZE) {
        int swap_block = (rand() % (BOARD_SIZE / SUB_BOARD_SIZE)) * SUB_BOARD_SIZE;
        if (block != swap_block) {
            for (int col = 0; col < SUB_BOARD_SIZE; ++col) {
                swap_columns(board->data, block + col, swap_block + col);
            }
        }
    }
}

/**
 * Swap two rows
*/
void swap_rows(int data[BOARD_SIZE][BOARD_SIZE], int row1, int row2) {
    for (int col = 0; col < BOARD_SIZE; ++col) {
        int temp = data[row1][col];
        data[row1][col] = data[row2][col];
        data[row2][col] = temp;
    }
}

/**
 * Swap two columns
*/
void swap_columns(int data[BOARD_SIZE][BOARD_SIZE], int col1, int col2) {
    for (int row = 0; row < BOARD_SIZE; ++row) {
        int temp = data[row][col1];
        data[row][col1] = data[row][col2];
        data[row][col2] = temp;
    }
}

/**
 * Save a Sudoku board to a file
*/
int save_sudoku_board(SudokuBoard *board, const char *file_name) {
    FILE *file = fopen(file_name, "w");

    if (file == NULL) {
        printf("Error opening the file %s\n", file_name);
        return 1;
    }

    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            fprintf(file, "%d", board->data[i][j]);
            if (j < BOARD_SIZE - 1) {
                fprintf(file, " ");
            }
        }
        fprintf(file, "\n");
    }

    fclose(file);
    return 0;
}

/**
 * Print a Sudoku board to the screen using only one stream to avoid getting stdout hijacked when executing in multiple
 * processes at once
 */
void print_sudoku_board(SudokuBoard *board) {
    char result[512];
    int offset = 0;

    for (int i = 0; i < BOARD_SIZE; ++i) {
        if (i % SUB_BOARD_SIZE == 0) {
            offset += sprintf(result + offset, "+---------+---------+---------+\n");
        }
        for (int j = 0; j < BOARD_SIZE; ++j) {
            if (j % SUB_BOARD_SIZE == 0) {
                offset += sprintf(result + offset, "|");
            }
            int val = board->data[i][j];

            if (val == 0) {
                offset += sprintf(result + offset, " - ");
            } else {
                offset += sprintf(result + offset, " %d ", val);
            }
        }
        offset += sprintf(result + offset, "|\n");
    }
    sprintf(result + offset, "+---------+---------+---------+\n");
    printf("%s", result);
}

/**
 * Duplicates a Sudoku board without allocating a new one
 */
void duplicate_board(SudokuBoard *original, SudokuBoard *copy) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            copy->data[i][j] = original->data[i][j];
        }
    }
}

/**
 * Remove numbers from a copy of the original Sudoku board
 */
void poke_holes_in_board(SudokuBoard *board, SudokuBoard *perforated_board) {
    duplicate_board(board, perforated_board);

    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if ((rand() % 101) > PERFORATION_CHANCE_PERCENTAGE) {
                perforated_board->data[i][j] = 0;
            }
        }
    }
}

/**
 * Process a solved solution, maybe someday print it to stdout or save it to a file.
 */
void process_solved_solution(SudokuBoard *board, SudokuBoardArray *solutions) {
    append_to_array(solutions, *board);
}

/**
 * Solve the Sudoku board using backtracking. Solve for all solutions.
 * @return If the board has a solution, return true. False otherwise.
 */
bool solve_backtrack(SudokuBoard *board, int *num_solutions, int row, int col, SudokuBoardArray *solutions) {
    if (row == BOARD_SIZE) {
        process_solved_solution(board, solutions);
        (*num_solutions)++;
        return true;
    }

    if (board->data[row][col] != 0) {
        if (col < BOARD_SIZE - 1) {
            solve_backtrack(board, num_solutions, row, col + 1, solutions);
        } else {
            solve_backtrack(board, num_solutions, row + 1, 0, solutions);
        }
    }

    for (int num = 1; num <= 9; num++) {
        if (is_valid(board, row, col, num)) {
            board->data[row][col] = num;
            if (col < BOARD_SIZE - 1) {
                solve_backtrack(board, num_solutions, row, col + 1, solutions);
            } else {
                solve_backtrack(board, num_solutions, row + 1, 0, solutions);
            }
            board->data[row][col] = 0;
        }
    }

    return false;
}

/**
 * Solve an incomplete Sudoku board
 * @return Array of solutions. Array size is the last parameter.
 *         This needs to be freed by the caller.
 */
SudokuBoardArray *solve(SudokuBoard *board) {
    int num_solutions = 0;
    SudokuBoardArray *arr = create_sudoku_board_array(NUM_SOLUTIONS);
    solve_backtrack(board, &num_solutions, 0, 0, arr);

    return arr;
}

/**
 * Scramble an array of numbers from 0 to 9
 */
void scramble_num_order(int *num_order) {
    int j, temp;
    for (int i = BOARD_SIZE - 1; i > 0; i--) {
        j = rand() % (i + 1);
        temp = num_order[i];
        num_order[i] = num_order[j];
        num_order[j] = temp;
    }
}

/**
 * Randomly generate a valid Sudoku board
 */
void generate_sudoku_board(SudokuBoard *result) {
    int num_order[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    scramble_num_order(num_order);
    generate_board(result, 0, 0, num_order);
}

SudokuBoardArray *create_sudoku_board_array(const int initial_capacity) {
    SudokuBoardArray *arr = malloc(sizeof(SudokuBoardArray));
    arr->array = malloc(initial_capacity * sizeof(SudokuBoard));
    arr->size = 0;
    arr->capacity = initial_capacity;
    return arr;
}

void append_to_array(SudokuBoardArray *arr, SudokuBoard board) {
    if (arr->size == arr->capacity) {
        arr->capacity *= 2;
        // printf("Reallocating from %d to %d\n", arr->size, arr->capacity);
        SudokuBoard *tmp_ptr = realloc(arr->array, arr->capacity * sizeof(SudokuBoard));
        if (tmp_ptr == NULL) {
            fprintf(stderr, "Memory allocation failed");
            free(arr->array);
            exit(1);
        }
        arr->array = tmp_ptr;
    }

    arr->array[arr->size] = board;
    arr->size++;
}

void free_array(SudokuBoardArray *arr) {
    free(arr->array);
    free(arr);
}
