#include "util.h"

/**
 * Write a log file and print the message to the CLI (if DEBUG is defined)
 */
void write_log(FILE *fp, const int proc_rank, const char *msg_fmt, ...) {
    va_list args, args_copy;
    va_start(args, msg_fmt);
    va_copy(args_copy, args);

    if (proc_rank > 0) {
        fprintf(fp, "[WORKER %d] ", proc_rank);
    } else {
        fprintf(fp, "[MASTER] ");
    }

    vfprintf(fp, msg_fmt, args);
    fprintf(fp, "\n");

#ifdef DEBUG
    if (proc_rank > 0) {
        printf(CLI_WORKER "[WORKER %d] " CLI_RESET, proc_rank);
    } else {
        printf(CLI_MASTER "[MASTER] " CLI_RESET);
    }
    vprintf(msg_fmt, args_copy);
    printf("\n");
#endif

    va_end(args);
    va_end(args_copy);
}
