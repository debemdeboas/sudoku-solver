#ifndef UTIL_H_
#define UTIL_H_

#include <stdarg.h>
#include <stdio.h>

#define CLI_GRN "\033[32m"  /* Green      */
#define CLI_YEL "\033[33m"  /* Yellow     */
#define CLI_BOLD "\033[1m"  /* Bold       */
#define CLI_RESET "\033[0m" /* Text Reset */
#define CLI_MASTER CLI_BOLD CLI_GRN
#define CLI_WORKER CLI_YEL CLI_BOLD

void write_log(FILE *fp, const int proc_rank, const char *msg_fmt, ...);

#endif // UTIL_H_