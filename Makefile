CC = gcc
CFLAGS = -Wall -Ofast
OBJDIR = obj
BINDIR = bin
SRCDIR = src
DEPS = $(wildcard $(SRCDIR)/*.h)
SRC = $(filter-out $(SRCDIR)/sudoku_solver.c $(SRCDIR)/main.c, $(wildcard $(SRCDIR)/*.c))
OBJS = $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(SRC))

all: dirs $(BINDIR)/main

debug: CFLAGS = -g -Wall -Og -DDEBUG
debug: clean all

wasm: CC = emcc
wasm: CFLAGS = -Wall -O2
wasm: clean dirs
wasm: $(BINDIR)/sudoku_solver.js

$(BINDIR)/main: $(OBJS) $(OBJDIR)/main.o
	$(CC) $(CFLAGS) -o $@ $^

$(OBJDIR)/%.o: $(SRCDIR)/%.c $(DEPS)
	$(CC) $(CFLAGS) -c $< -o $@

$(BINDIR)/sudoku_solver.js: $(OBJS) $(OBJDIR)/sudoku_solver.o
	$(CC) $(CFLAGS) -o $@ $^ \
		-sMODULARIZE -sEXPORT_ES6 -sEXPORTED_RUNTIME_METHODS=ccall

.PHONY: clean dirs
clean:
	rm -rf $(OBJDIR)/* $(BINDIR)/*

dirs:
	@mkdir -p $(BINDIR) $(OBJDIR)
